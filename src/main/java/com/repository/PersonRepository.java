package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.models.Person;

// This will be AUTO IMPLEMENTED by Spring into a Bean called personRepository
// CRUD refers Create, Read, Update, Delete

public interface PersonRepository extends JpaRepository<Person, Integer> {

}