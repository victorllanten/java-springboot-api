package com.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.models.Person;
import com.repository.PersonRepository;

@RestController
@RequestMapping(path="/persons") 
public class PersonsController {

	@Autowired
	PersonRepository repo;
	
	@GetMapping(value = { "", "/" })
	public @ResponseBody List<Person>  persons_get_all()  {
		return repo.findAll();
	}
	
	@GetMapping("/{person_id}")
	public @ResponseBody Optional<Person>  persons_get(@PathVariable Integer person_id)  {
		return repo.findById(person_id);
	}

	@PostMapping("/")
	public Person persons_post(@RequestBody Person person) {
		return repo.save(person);
	}

	@PutMapping("/{person_id}")
	public Person persons_put(@PathVariable("person_id") Integer person_id, @RequestBody Person person) {
		person.setId(person_id);
		return repo.save(person);
	}

	@DeleteMapping("/{person_id}")
	public String persons_delete(@PathVariable("person_id") Integer person_id) {
		 repo.deleteById(person_id);
		 return "true";
	}

}
